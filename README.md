# Pendu peda

Homepage: https://github.com/CyrilleBiot/pendu-peda-gtk
Description: Hangman game with as themes the learning areas of cycles 2 and 3 of primary school. Pendu-peda-gtk does not take any options. Thematic files can be incremented in the directory : /home/$user/.primtux/pendu-peda-gtk/data-files/